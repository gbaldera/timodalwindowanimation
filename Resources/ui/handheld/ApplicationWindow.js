//Application Window Component Constructor
function ApplicationWindow() {
	//load component dependencies
	var FirstView = require('ui/common/FirstView');

	//create component instance
	var self = Ti.UI.createWindow({
		backgroundColor : '#ffffff',
		borderRadius : 6
	});

	//construct UI
	var firstView = new FirstView();
	self.add(firstView);

	var button = Ti.UI.createButton({
		title : "Do animation and open modal",
		width : 300,
		left : 10,
		height : 45,
		bottom : 20

	});

	self.add(button);

	button.addEventListener("click", buttonHandler);

	var t1, anim;

	function buttonHandler() {

		goBackAnim();

		var w = Ti.UI.createWindow({
			backgroundColor : "#ddd",
			title : "merry xmas",

		});

		var closeButton = Ti.UI.createButton({
			title : "Close",
			width : 300,
			left : 10,
			height : 45,
			bottom : 20
		});

		w.add(closeButton);

		closeButton.addEventListener("click", function() {
			w.close();
			comeForwardAnim();
		});

		w.open({
			modal : true,

		});
	}

	function goBackAnim() {
		t1 = Ti.UI.create3DMatrix();
		//this one is for perspectivity.. you can play with that.
		t1.m34 = 1.0 / -400;
		t1 = t1.rotate(14, 1, 0, 0);
		t1 = t1.scale(0.90, 0.90, 0.90);
		t1 = t1.translate(0, 20, 0);

		anim = Ti.UI.createAnimation({
			transform : t1,
			duration : 400,
			curve : Ti.UI.ANIMATION_CURVE_EASE_OUT,
			opacity : 0.6
		});

		self.animate(anim);
	}

	function comeForwardAnim() {
		t1 = Ti.UI.create3DMatrix();

		self.animate({
			transform : t1,
			duration : 500,
			curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT,
			opacity : 1
		});
	}

	return self;
}

//make constructor function the public component interface
module.exports = ApplicationWindow;
