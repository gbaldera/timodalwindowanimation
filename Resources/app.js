/*
* Single Window Application Template:
* A basic starting point for your application.  Mostly a blank canvas.

TiModalWindowAnimation - like in GMAIL app

made by artanis

only in iOS

feel to free to use

*/

//bootstrap and check dependencies
if (Ti.version < 1.8) {
	alert('Sorry - this application template requires Titanium Mobile SDK 1.8 or later');
}

// This is a single context application with mutliple windows in a stack
(function() {
	//determine platform and form factor and render approproate components
	var osname = Ti.Platform.osname, version = Ti.Platform.version, height = Ti.Platform.displayCaps.platformHeight, width = Ti.Platform.displayCaps.platformWidth;

	//considering tablet to have one dimension over 900px - this is imperfect, so you should feel free to decide
	//yourself what you consider a tablet form factor for android
	var isTablet = osname === 'ipad' || (osname === 'android' && (width > 899 || height > 899));

	var Window;

	// Android uses platform-specific properties to create windows.
	// All other platforms follow a similar UI pattern.

	Window = require('ui/handheld/ApplicationWindow');

	new Window().open();
})();
